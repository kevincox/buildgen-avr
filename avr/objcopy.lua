-- Copyright 2011-2012 Kevin Cox

--[[---------------------------------------------------------------------------]
[                                                                              ]
[  Permission is hereby granted, free of charge, to any person obtaining a     ]
[  copy of this software and associated documentation files (the "Software"),  ]
[  to deal in the Software without restriction, including without limitation   ]
[  the rights to use, copy, modify, merge, publish, distribute, sublicense,    ]
[  and/or sell copies of the Software, and to permit persons to whom the       ]
[  Software is furnished to do so, subject to the following conditions:        ]
[                                                                              ]
[  The above copyright notice and this permission notice shall be included in  ]
[  all copies or substantial portions of the Software.                         ]
[                                                                              ]
[  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  ]
[  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    ]
[  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     ]
[  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  ]
[  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     ]
[  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         ]
[  DEALINGS IN THE SOFTWARE.                                                   ]
[                                                                              ]
[-----------------------------------------------------------------------------]]

L.avr = L.avr or {}
L.avr.objcopy = {}
if not P.L.avr then P.L.avr = {} end
if not P.L.avr.objcopy then P.L.avr.objcopy = {} end

S.import "stdlib"

do -- So that we can hide our locals.
local state = {}

--- Create new objcopy state
-- Creates and returns an opaque state.  This state is a table and is therefore
-- passed by refrence.
--
-- @return The newly created state.
function L.avr.objcopy.newState ( )
	data = {
		arguments = T.List(),
		removes   = T.List(),
	}

	return data
end

--- Stashes the current state.
-- Returns the current state and loads a new state.  This is equivilent to
-- L.avr.objcopy.swapState(L.avr.objcopy.newState()).
--
-- @return The old state.
function L.avr.objcopy.stashState ( )
	return L.avr.objcopy.swapState(L.avr.objcopy.newState())
end

--- Swap the state
-- Swaps new with the current state.
--
-- @param new The new state to load.
-- @return The old state.
function L.avr.objcopy.swapState ( new )
	local old = state

	L.avr.objcopy.loadState(new)

	return old
end

--- Load a state
-- Loads the state data
--
-- @param data The state to load.
function L.avr.objcopy.loadState ( data )
	state = data
end

L.avr.objcopy.swapState(L.avr.objcopy.newState())

--- Set the format of the output file.
--
-- This format is passed to the ##-O## flag of avr-objcopy.
function L.avr.objcopy.format ( fmt )
	state.outformat = fmt
end

--- Remove a section.
--
-- Remove a section from the output.  This is passed to the -R flag of
-- avr-ojcopy.  These are added on subsequent calls and can be cleard with
-- L.avr.objcopy.clearRemoves().
--
-- @param fmt The section to remove or a table of sections to remove.
function L.avr.objcopy.remove ( sec )
	if type(sec) ~= "table" then
		sec = {tostring(sec)}
	end

	state.removes:extend(sec)
end

--- Remove all removes
--
-- This clears all sections to remove from the remove list.
function L.avr.objcopy.clearRemoves ( )
	state.removes = T.List()
end

--- Perform the copy.
-- Coppies src to dest.
function L.avr.objcopy.copy ( src, dest )
	src  = C.path(src)
	dest = C.path(dest)

	local cmd = T.List{"*avr-objcopy"}

	if state.outformat then
		cmd:extend{"-O", state.outformat}
	end

	for r in state.removes:iter() do
		cmd:extend{"-R", r}
	end

	cmd:extend{src, dest}

	C.addGenerator({src}, cmd, {dest}, {
		description = "Translating "..src.." to "..dest,
	})

	return dest
end

end
