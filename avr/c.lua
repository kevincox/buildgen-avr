-- Copyright 2011-2012 Kevin Cox

--[[---------------------------------------------------------------------------]
[                                                                              ]
[  Permission is hereby granted, free of charge, to any person obtaining a     ]
[  copy of this software and associated documentation files (the "Software"),  ]
[  to deal in the Software without restriction, including without limitation   ]
[  the rights to use, copy, modify, merge, publish, distribute, sublicense,    ]
[  and/or sell copies of the Software, and to permit persons to whom the       ]
[  Software is furnished to do so, subject to the following conditions:        ]
[                                                                              ]
[  The above copyright notice and this permission notice shall be included in  ]
[  all copies or substantial portions of the Software.                         ]
[                                                                              ]
[  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  ]
[  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    ]
[  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     ]
[  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  ]
[  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     ]
[  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         ]
[  DEALINGS IN THE SOFTWARE.                                                   ]
[                                                                              ]
[-----------------------------------------------------------------------------]]

L.avr = L.avr or {}
L.avr.c = {}

S.import "stdlib"
L.import "avr.ld"

if not P.L.avr then P.L.avr = {} end
if not P.L.avr.c then P.L.avr.c = {} end

do -- So that we can hide our locals.
local state = {}

--- The optimization level.
--
-- A string value representing the level of optimization to use when building
-- the project. Possible values are:
-- <ul><li>
--		none - Perform no optimization.
--</li><li>
--		quick - Perform light optimization.
--</li><li>
--		regular - Perform regular optimization.
--</li><li>
--		full - Fully optimize the executable.
--</li><li>
--		max - Optimize as much as possible (possibly experimental optimizations).
--</li></ul>
L.avr.c.optimization = "regular"
if D.debug then L.avr.c.optimization = "none" end

--- Whether to profile.
--
-- If true profiling code will be present in the resulting executable. This
-- value defaults to true if D.debug is set otherwise false.
L.avr.c.profile = false
if D.debug then L.avr.c.profile = true end

--- Whether to produce debugging symbols.
-- If true debugging symbols will be produced in the resulting executable. This
-- value defaults to true if D.debug is set otherwise false.
L.avr.c.debug = false
if D.debug then L.avr.c.debug = true end

--- Create new c state
-- Creates and returns an opaque state.  This state is a table and is therefore
-- passed by refrence.
--
-- @return The newly created state.
function L.avr.c.newState ( )
	local data = {
		arguments = T.List(),
		linker    = L.avr.ld.newState(),
		mmcu      = D["avr.mmcu"],
		f_cpu     = D["avr.f_cpu"],
	}

	return data
end

--- Stashes the current state.
-- Returns the current state and loads a new state.  This is equivilent to
-- L.avr.c.swapState(L.avr.c.newState()).
--
-- @return The old state.
function L.avr.c.stashState ( )
	return L.avr.c.swapState(L.avr.c.newState())
end

--- Swap the state
-- Swaps new with the current state.
--
-- @param new The new state to load.
-- @return The old state.
function L.avr.c.swapState ( new )
	local old = state

	old.debug        = L.avr.c.debugOveride
	old.optimization = L.avr.c.optimizationOveride
	old.profile      = L.avr.c.profileOveride

	L.avr.c.loadState(new)

	return old
end

--- Load a state
-- Loads the state data
--
-- @param data The state to load.
function L.avr.c.loadState ( data )
	state = data

	L.avr.c.debugOveride        = data.debug
	L.avr.c.optimizationOveride = data.optimization
	L.avr.c.profileOveride      = data.profile
end

L.avr.c.swapState(L.avr.c.newState())

if not P.L.avr.c.compiler then
	local compilers = {
		{	name = "avr-gcc", -- Name of the executable
			flags = {
				compile  = "-c",
				mmcu     = "-mmcu=%s",
				output   = {"-o", "%s"}, -- the option to set the output file name.
				debug    = "-g",         -- the option to enable debug mode.
				profile  = "-p",         -- the option to enable profiling.
				include  = {"-I", "%s"}, -- the option to add an include directory.
				define   = {"-D%s"}, -- the option to add an include directory.
				optimize = {             -- Flags for different levels of optimization.
					none    = {},
					quick   = "-O",
					regular = "-O2",     -- Default optimazation.
					full    = "-O3",
					max     = {
					            "-O3",
					            "-fexpensive-optimizations",
					            "-fomit-frame-pointer"
					          },     -- Highest possoble (possibly exparemental)
				}
			}
		},
	}
	T.List(compilers) -- turn tabe into a penlight 'list'

	local compiler;
	for c in compilers:iter() do          -- Find the first compiler they have
		if S.findExecutable(c.name) then -- installed on thier system.
			compiler = c
			compiler.name = S.findExecutable(compiler.name)

			break
		end
	end

	if compiler == nil then
		error("Error: No C compiler found.", 0)
	else
		P.L.avr.c.compiler = compiler
	end
end

-- Overide the default optimization level.
L.avr.c.optimizationOveride = L.avr.c.optimizationOveride

-- Overide the default profile setting.
L.avr.c.profileOveride = L.avr.c.profileOveride

-- Overide the default profile setting.
L.avr.c.debugOveride = L.avr.c.debugOveride

--- Add an argrment.
-- Add an argument to the compiler command line.  Please try to avoid using this
-- as it is not portable across compilers.  Please use the other functions that
-- modify the command line (L.avr.ch as L.avr.c.optimization and L.avr.c.define()) as they
-- are localized to the compiler being used.
--
-- @param args a string or list of strings to be added to the compiler command
--	line.
function L.avr.c.addArg ( args )
	if type(args) ~= "table" then
		args = T.List{tostring(args)}
	else
		args = T.List(args)
	end

	for a in args:iter() do state.arguments:append(a) end
end

---- The mmcu to compile for.
--
-- This is passed to the ##-mmcu## flag of gcc.  See the avr-gcc documentation
-- for the values you can use.
function L.avr.c.mmcu ( mmcu )
	state.mmcu = mmcu
end

-- The CPU Frequecy to define.
--
-- This is defined as ##F_CPU## and affects timing.
function L.avr.c.f_cpu ( f_cpu )
	state.f_cpu = f_cpu
end

--- Add an include directory
--
-- @param dir an string or list of strings.  These will be treated as BuildGen
--	paths.
function L.avr.c.addInclude ( dir )
	if type(dir) ~= "table" then
		dir = {tostring(dir)}
	end

	for k, v in pairs(dir) do
		v = C.path(v)
		for l, w in pairs(P.L.avr.c.compiler.flags.include) do
			L.avr.c.addArg(w:format(v))
		end
	end
end

--- Define a macro
-- Define a macro during compliation.
--
-- @param map A table of key/value pairs to be defined during compilation.
function L.avr.c.define ( map )
	if type(map) ~= "table" then
		map = {tostring(map)}
	end

	for k, v in pairs(map) do
		if type(v) ~= "string" then
			v = ""
		else
			v = "="..v
		end
		for l, w in pairs(P.L.avr.cpp.compiler.flags.define) do
			L.avr.cpp.addArg(w:format(k..v))
		end
	end
end

--- Link a library.
--
-- This just calls L.avr.ld.addLib() with the linker being used by L.avr.c.
--
-- @param dir a string or list of strings as the name of the libraries.
function L.avr.c.addLib ( lib )
	local ln = L.avr.ld.swapState(state.linker)

	L.avr.ld.addLib(lib)

	state.linker = L.avr.ld.swapState(ln)
end

--- Compile a source into an object.
--
-- @prarm src The file to compile.
-- @ headers A list of headers that are needed.
-- @param obj The place to put the resulting object file.
function L.avr.c.compileObject (src, headers, obj)
	obj = C.path(obj)
	src = C.path(src)
	headers = T.List(headers):map(C.path)
	headers:append(src) -- We depend on the source file too.

	local compiler = P.L.avr.c.compiler

	local oldarguments = state.arguments
	state.arguments = T.List()

	L.avr.c.addArg(compiler.name)
	L.avr.c.addArg(compiler.flags.compile)

	L.avr.c.addArg(oldarguments)

	if state.mmcu then
		L.avr.c.addArg(compiler.flags.mmcu:format(state.mmcu))
	end

	if state.f_cpu then
		L.avr.cpp.define{F_CPU = state.f_cpu.."UL"}
	end

	local debug = L.avr.c.debugOveride
	if debug == nil then debug = L.avr.c.debug end
	if debug then                      -- Add the debug flag.
		L.avr.c.addArg(compiler.flags.debug)
	end
	local profile = L.avr.c.profileOveride
	if profile == nil then profile = L.avr.c.profile end
	if profile then                    -- Add the profile flag.
		L.avr.c.addArg(compiler.flags.profile)
	end
	local optimization = L.avr.c.optimizationOveride
	if optimization == nil then optimization = L.avr.c.optimization end
	local o = compiler.flags.optimize[optimization] -- Set the optimization
	if o then                                       -- level.                                --
		L.avr.c.addArg(o)                               --
	end                                             --

	for i in T.List(compiler.flags.output):iter() do -- Add the desired output
		L.avr.c.addArg(i:format(obj))                    -- file to the command
	end                                              -- line.

	L.avr.c.addArg(src)

	C.addGenerator(headers, state.arguments, {obj}, {
		description = "Compiling "..obj
	})

	state.arguments = oldarguments;
end

local function compile ( linkfunc, sources, out )
	out = C.path(out)
	sources = T.List(sources):map(C.path)

	local ln = L.avr.ld.swapState(state.linker) -- Use our linker

	local projectRoot = C.path("<") -- Cache this.
	local outRoot     = C.path(">") --

	local h, s, l = T.List(), T.List(), T.List()
	local objects = T.List()

	for source in sources:iter() do
		if source:match"\.[Hh]$" then
			h:append(source)
		elseif source:match"\.a$" then
			l:append(source)
		elseif source:match"\.o$" then
			objects:append(source)
		else
			s:append(source)
		end
	end

	for source in s:iter() do
		source = C.path(source)

		local object = C.path("@"..source..".o")

		L.avr.c.compileObject(source, h, object)
		objects:append(object)
	end
	objects:extend(l)

	L.avr.ld.mmcu(state.mmcu)

	out = linkfunc(objects, out)

	state.linker = L.avr.ld.swapState(ln) -- Put their linker back.

	return out
end

--- Compile an Executable
-- Compiles and links a list of files into executables.
--
-- @param sources A list of sources (bot header and source files) that will be
--   used when compiling the executable.
-- @param out The file to be created.  ".exe" will be appended if compiling on
--   Windows.
-- @returns The actual path of the created executable.
function L.avr.c.compile ( sources, out )
	return compile(L.avr.ld.link, sources, out)
end

--- Compile a Static Library
-- Compiles and links a list of files into a static library.
--
-- @param sources A list of sources (bot header and source files) that
--   will be used when compiling the library.
-- @param out The file to be created.  This is just the basename,
-- @returns The actual path used for the output executable.
function L.avr.c.compileStatic ( sources, out )
	return compile(L.avr.ld.linkStatic, sources, out)
end

--- Compile a Shared Library
-- Compiles and links a list of files into a shared library.
--
-- @param sources A list of sources (bot header and source files) that
--   will be used when compiling the library.
-- @param out The file to be created.  This is just the basename,
-- @returns The actual path used for the output executable.
function L.avr.c.compileShared ( sources, out )
	return compile(L.avr.ld.linkShared, sources, out)
end

end
