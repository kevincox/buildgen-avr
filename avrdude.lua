-- Copyright 2011 Kevin Cox

--[[---------------------------------------------------------------------------]
[                                                                              ]
[  Permission is hereby granted, free of charge, to any person obtaining a     ]
[  copy of this software and associated documentation files (the "Software"),  ]
[  to deal in the Software without restriction, including without limitation   ]
[  the rights to use, copy, modify, merge, publish, distribute, sublicense,    ]
[  and/or sell copies of the Software, and to permit persons to whom the       ]
[  Software is furnished to do so, subject to the following conditions:        ]
[                                                                              ]
[  The above copyright notice and this permission notice shall be included in  ]
[  all copies or substantial portions of the Software.                         ]
[                                                                              ]
[  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  ]
[  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    ]
[  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     ]
[  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  ]
[  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     ]
[  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         ]
[  DEALINGS IN THE SOFTWARE.                                                   ]
[                                                                              ]
[-----------------------------------------------------------------------------]]


L.avrdude = {}

if not P.L.avrdude then P.L.avrdude = {} end

do -- So that we can hide our locals.
local state = {}

--- Create new qt state
-- Creates and returns an opaque state.  This state is a table and is therefore
-- passed by refrence.
--
-- @return The newly created state.
function L.avrdude.newState ( )
	local data = {
		partno = D["avrdude.partno"] or "auto",
		port   = D["avrdude.port"],
		progid = D["avrdude.progid"],
		baud   = D["avrdude.baud"],

		verify = not D["debug"],
	}

	return data
end

--- Stashes the current state.
-- Returns the current state and loads a new state.  This is equivilent to
-- L.qt.swapState(L.qt.newState()).
--
-- @return The old state.
function L.avrdude.stashState ( )
	return L.avrdude.swapState(L.avrdude.newState())
end

--- Swap the state
-- Swaps new with the current state.
--
-- @param new The new state to load.
-- @return The old state.
function L.avrdude.swapState ( new )
	local old = state

	L.avrdude.loadState(new)

	return old
end

--- Load a state
-- Loads the state data
--
-- @param data The state to load.
function L.avrdude.loadState ( data )
	state = data
end

--- Verify uploaded data
--
-- This defaults to false when D.debug is true otherwise false.
function L.avrdude.verfiy ( verify )
	state.verfiy = verify
end

--- Set the part number to program.
--
-- This is a number from the avrdude part number list.  The default can be
-- set via ##D["avrdude.partno"]##.
function L.avrdude.partno ( num )
	state.partno = num
end

--- Set the port.
-- This is the port to program from.  This is passed to the ##-P## option in the
-- avrdude commandline.  This can be set via ##D["avrdude.port"]##.
function L.avrdude.port ( port )
	state.port = port
end

--- Set the Programmer ID
-- This sets the programmer to use.  This is passed to the ##-c## option in the
-- avrdude commandline.  This can be set via ##D["avrdude.port"]##.
function L.avrdude.progid ( progid )
	state.progid = progid
end

--- Set the BAUD
-- This sets the BAUD to use.  This is passed to the ##-b## option in the
-- avrdude commandline.  This can be set via ##D["avrdude.baud"]##.
function L.avrdude.baud ( baud )
	state.baud = baud
end

--- Perform a memory operation.
--
-- This corisponds to the ##-U## operation for avrdude.  Check the avrdude
-- documentation for more information.
--
-- @param mem The memory type to operate on.
-- @param op The operation to perform. (##r##ead, ##w##rite, ##v##erify)
-- @param file The file to use in conjunction with the mcu.
-- @param fmt The format of the file to read/write.  Defaults to auto.
-- @return A step object.  This can be passed to L.avrdude.perform to create a
--	build target.
function L.avrdude.memoryop ( mem, op, file, fmt )
	file = C.path(file)

	local cmd = T.List{"*avrdude", "-p", state.partno}

	if state.port then
		cmd:append "-P"
		cmd:append(state.port)
	end

	if state.progid then
		cmd:append "-c"
		cmd:append(state.progid)
	end

	if state.baud then
		cmd:append "-b"
		cmd:append(state.baud)
	end

	if not state.verfiy then
		cmd:append "-V"
	end

	cmd:append("-U")
	cmd:append(mem..":"..op..":"..file..":"..(fmt or "a"))

	return {cmd = cmd, depends = T.List{file}}
end

--- Create a build target from the steps.
--
-- The functions in L.avrdude return step objects instead of creating build
-- targets.  This is because it is difficult to track changes to the data on a
-- mcu.  To create a target this function is used.
--
-- It takes a list of steps and creates a target that executes them in order.
--
-- @param steps A list of step objects.
-- @param target The name to give to the target.  This target will be a magic
--	target.
function L.avrdude.perform ( steps, target )
	local steps = T.List(steps)

	local cmds = T.List{}
	local deps = T.List{}

	for s in steps:iter() do
		cmds:append(s.cmd)
		deps:extend(s.depends)
	end

	C.addGenerator(deps, cmds, {target}, {
		description = "Programing target",
		magic = true, -- The target is magic.
	})

	return target
end

L.avrdude.swapState(L.avrdude.newState())
end
